# Techshare Elasticsearch

## Présenté le 13 février 2019 dans l'auditorium de Prisma Media

### Retours sur la formation de Bo Andersen sur Elasticsearch 5.4

???

## Assets

[[/](https://www.udemy.com/elasticsearch-complete-guide/)] Formation Udemy visionnée

[[/](https://remarkjs.com)] Remarkjs

[[/](https://mermaidjs.github.io)] Mermaid

## Setup pour un environnement de développement

Docker doit, au préalable, être installé et lancé.

Vous pouvez initialiser l'environnement suivant la méthode de la formation (la version d'ES et Kibana sera 5.4) :

- Cloner le [repository](https://github.com/codingexplained/complete-guide-to-elasticsearch) associé à la formation
- Exécuter :
    - `cd /path/to/complete-guide-to-elasticsearch`
    - `docker-compose up`

Sinon, vous pouvez créer votre propre environnement, par contre les exemples dans les notes ne sont pas compatibles ES 6.x :

- `docker network create elasticsearch`
- `docker run -d --name elasticsearch --net elasticsearch -p 9200:9200 elastic/elasticsearch:6.5.1`
- `docker run -d --name kibana --net elasticsearch -p 5601:5601 kibana:6.5.1`

ES sera accessible à l'adresse `http://localhost:9200` et Kibana à l'adresse `http://localhost:5601`

---
name: sommaire-1

## Sommaire

.left-column[
### Partie I
]

.right-column[
- Analyzer
    - Schéma
    - Inverted Index

- Mapping
    - Mapping dynamique
    - Types
    - Tips
]

---
name: sommaire-2

## Sommaire

.left-column[
### Partie I
### Partie II
]

.right-column[
- Recherches
    - Query vs Filter
    - Scoring
    - Requête booléenne
    - Slop
    - Fuzziness
    - Query String
    - Informations diverses
    - Tips

- Aggrégations
    - Scalaires ; Buckets
    - Contexte
]

---
name: sommaire-3

## Sommaire

.left-column[
### Partie I
### Partie II
### Partie III
]

.right-column[
- Infrastructure
    - Vue d'ensemble
    - Cluster ; Node
    - Shard ; Replica
    - Schéma

- Elastic Stack
    - X-Pack
    - Kibana
    - Beats
    - Logstash
]

---
name: analyzer-structure
class: center

### Analyzer

.third-column[
<div class="mermaid">
graph TB
    subgraph Entrée
    T(Texte)
    end
    subgraph Analyzer
    C(0,* Character Filter) --> O(1 Tokenizer)
    O --> F(0,* Token Filter)
    end
    subgraph Sortie
    I(Inverted Index)
    end
    T --> C
    F --> I
</div>
]

.third-column[
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>

#### Character Filter

Il effectue des opérations sur le texte brut.

#### Tokenizer

Il transforme du texte en token(s) (`term`)

#### Token Filter
 
Il effectue des opérations sur les tokens
]

.third-column[
#### Exemple

`<p>Elasticsearch's Techshare</p>`

⬇️ **html_strip** (Char. Filter)

`Elasticsearch's Techshare`

⬇️ **whitespace** (Tokenizer)

[Elasticsearch's]

[Techshare]

⬇️ **lowercase** (Token Filter)

[elasticsearch's]

[techshare]
]

???

## Analyzer

Liste des **Analyzers** disponibles dans ES :
- Standard
- Simple, texte découpé suivant caractères qui ne sont pas des lettres + lowercase
- Stop, comme Simple mais enlève également les stop word
- Keyword
- Pattern, découpe suivant une expression régulière (\W+ par défaut)
- Whitespace

.need-ref[Ce fut une pratique répendue que de gérer les stop words. Dorénavant ES a amélioré son algorithme de pertinence et il n'est donc plus nécessaire, voire contre-productif, de vouloir prendre la main sur la gestion des stopwords.]
<sup>[réf. nécessaire]</sup>

### Standard

```
POST _analyze
{
	"analyzer": "standard",
	"text": "Speedrun ; fléau ou pas ? Aujourd'hui, une réponse !"
}

Retour :
{
    "tokens": [
        {
            "token": "speedrun",
            "start_offset": 0,
            "end_offset": 8,
            "type": "<ALPHANUM>",
            "position": 0
        },
        {
            "token": "fléau",
            "start_offset": 11,
            "end_offset": 16,
            "type": "<ALPHANUM>",
            "position": 1
        },
        {
            "token": "ou",
            "start_offset": 17,
            "end_offset": 19,
            "type": "<ALPHANUM>",
            "position": 2
        },
        {
            "token": "pas",
            "start_offset": 20,
            "end_offset": 23,
            "type": "<ALPHANUM>",
            "position": 3
        },
        {
            "token": "aujourd'hui",
            "start_offset": 26,
            "end_offset": 37,
            "type": "<ALPHANUM>",
            "position": 4
        },
        {
            "token": "une",
            "start_offset": 39,
            "end_offset": 42,
            "type": "<ALPHANUM>",
            "position": 5
        },
        {
            "token": "réponse",
            "start_offset": 43,
            "end_offset": 50,
            "type": "<ALPHANUM>",
            "position": 6
        }
    ]
}
```

### Simple

```
POST _analyze
{
	"analyzer": "simple",
	"text": "Speedrun ; fléau ou pas ? Aujourd'hui, une réponse !"
}

Retour :
{
    "tokens": [
        {
            "token": "speedrun",
            "start_offset": 0,
            "end_offset": 8,
            "type": "word",
            "position": 0
        },
        {
            "token": "fléau",
            "start_offset": 11,
            "end_offset": 16,
            "type": "word",
            "position": 1
        },
        {
            "token": "ou",
            "start_offset": 17,
            "end_offset": 19,
            "type": "word",
            "position": 2
        },
        {
            "token": "pas",
            "start_offset": 20,
            "end_offset": 23,
            "type": "word",
            "position": 3
        },
        {
            "token": "aujourd",
            "start_offset": 26,
            "end_offset": 33,
            "type": "word",
            "position": 4
        },
        {
            "token": "hui",
            "start_offset": 34,
            "end_offset": 37,
            "type": "word",
            "position": 5
        },
        {
            "token": "une",
            "start_offset": 39,
            "end_offset": 42,
            "type": "word",
            "position": 6
        },
        {
            "token": "réponse",
            "start_offset": 43,
            "end_offset": 50,
            "type": "word",
            "position": 7
        }
    ]
}
```

### Stop

```
POST _analyze
{
	"analyzer": "stop",
	"text": "Speedrun ; problem or not ? Today, an answer !"
}

Retour : 
{
    "tokens": [
        {
            "token": "speedrun",
            "start_offset": 0,
            "end_offset": 8,
            "type": "word",
            "position": 0
        },
        {
            "token": "problem",
            "start_offset": 11,
            "end_offset": 18,
            "type": "word",
            "position": 1
        },
        {
            "token": "today",
            "start_offset": 28,
            "end_offset": 33,
            "type": "word",
            "position": 4
        },
        {
            "token": "answer",
            "start_offset": 38,
            "end_offset": 44,
            "type": "word",
            "position": 6
        }
    ]
}
```

### Keyword

```
POST _analyze
{
	"analyzer": "keyword",
	"text": "Speedrun ; fléau ou pas ? Aujourd'hui, une réponse !"
}

Retour :
{
    "tokens": [
        {
            "token": "Speedrun ; fléau ou pas ? Aujourd'hui, une réponse !",
            "start_offset": 0,
            "end_offset": 52,
            "type": "word",
            "position": 0
        }
    ]
}
```

### Pattern

```
POST _analyze
{
	"analyzer": "pattern",
	"text": "Speedrun ; fléau ou pas ? Aujourd'hui, une réponse !"
}

Retour :
{
    "tokens": [
        {
            "token": "speedrun",
            "start_offset": 0,
            "end_offset": 8,
            "type": "word",
            "position": 0
        },
        {
            "token": "fl",
            "start_offset": 11,
            "end_offset": 13,
            "type": "word",
            "position": 1
        },
        {
            "token": "au",
            "start_offset": 14,
            "end_offset": 16,
            "type": "word",
            "position": 2
        },
        {
            "token": "ou",
            "start_offset": 17,
            "end_offset": 19,
            "type": "word",
            "position": 3
        },
        {
            "token": "pas",
            "start_offset": 20,
            "end_offset": 23,
            "type": "word",
            "position": 4
        },
        {
            "token": "aujourd",
            "start_offset": 26,
            "end_offset": 33,
            "type": "word",
            "position": 5
        },
        {
            "token": "hui",
            "start_offset": 34,
            "end_offset": 37,
            "type": "word",
            "position": 6
        },
        {
            "token": "une",
            "start_offset": 39,
            "end_offset": 42,
            "type": "word",
            "position": 7
        },
        {
            "token": "r",
            "start_offset": 43,
            "end_offset": 44,
            "type": "word",
            "position": 8
        },
        {
            "token": "ponse",
            "start_offset": 45,
            "end_offset": 50,
            "type": "word",
            "position": 9
        }
    ]
}
```

### Whitespace

```
POST _analyze
{
	"analyzer": "whitespace",
	"text": "Speedrun ; fléau ou pas ? Aujourd'hui, une réponse !"
}

Retour :
{
    "tokens": [
        {
            "token": "Speedrun",
            "start_offset": 0,
            "end_offset": 8,
            "type": "word",
            "position": 0
        },
        {
            "token": ";",
            "start_offset": 9,
            "end_offset": 10,
            "type": "word",
            "position": 1
        },
        {
            "token": "fléau",
            "start_offset": 11,
            "end_offset": 16,
            "type": "word",
            "position": 2
        },
        {
            "token": "ou",
            "start_offset": 17,
            "end_offset": 19,
            "type": "word",
            "position": 3
        },
        {
            "token": "pas",
            "start_offset": 20,
            "end_offset": 23,
            "type": "word",
            "position": 4
        },
        {
            "token": "?",
            "start_offset": 24,
            "end_offset": 25,
            "type": "word",
            "position": 5
        },
        {
            "token": "Aujourd'hui,",
            "start_offset": 26,
            "end_offset": 38,
            "type": "word",
            "position": 6
        },
        {
            "token": "une",
            "start_offset": 39,
            "end_offset": 42,
            "type": "word",
            "position": 7
        },
        {
            "token": "réponse",
            "start_offset": 43,
            "end_offset": 50,
            "type": "word",
            "position": 8
        },
        {
            "token": "!",
            "start_offset": 51,
            "end_offset": 52,
            "type": "word",
            "position": 9
        }
    ]
}
```

# Character Filter

Liste des **Character Filter** disponibles dans ES :
- **Html Strip Character Filter** (html_strip), qui supprime les balises html et décode les entités html (commençant par &)
- **Mapping Character Filter** (mapping), qui remplace des valeurs par des autres
- **Pattern Replace Character Filter** (pattern_replace), pareil que Mapping Character Filter mais avec des expressions régulières

## Html Strip Character Filter

```
POST _analyze
{
	"char_filter": ["html_strip"],
	"tokenizer": "keyword",
	"text": "<p>Banjo-Kazooie</p>"
}

Retour :
{
    "tokens": [
        {
            "token": "\nBanjo-Kazooie\n",
            "start_offset": 0,
            "end_offset": 20,
            "type": "word",
            "position": 0
        }
    ]
}
```

## Mapping Character Filter

```
PUT {{index}}
{
	"settings": {
		"analysis": {
			"char_filter": {
				"word_to_emoji_char_filter": {
					"type": "mapping",
					"mappings": [
					    "goat => 🐐"
					]
				}
			}
		}
	}
}

POST {{index}}/_analyze
{
	"char_filter": ["word_to_emoji_char_filter"],
	"tokenizer": "keyword",
	"text": "goat simulator"
}

Retour :
{
    "tokens": [
        {
            "token": "🐐 simulator",
            "start_offset": 0,
            "end_offset": 14,
            "type": "word",
            "position": 0
        }
    ]
}
```

## Pattern Replace Character Filter
#### _(un antislash a été ajouté pour que le dollar apparaisse sur le navigateur)_

```
PUT {{index}}
{
	"settings": {
		"analysis": {
			"analyzer": {
				"yt_analyzer": {
					"type": "custom",
					"char_filter": [
						"yt_char_filter"
					],
					"tokenizer": "keyword"
				}
			},
			"char_filter": {
				"yt_char_filter": {
					"type": "pattern_replace",
					"pattern": "https:\/\/www.youtube.com\/watch.v=([a-zA-Z0-9]+)",
					"replacement": "https://www.youtube-redirector.com/video=\$1"
				}
			}
		}
	}
}

POST {{index}}/_analyze
{
	"analyzer": "yt_analyzer",
	"text": "Speedrun de Goat Simulator disponible ici : https://www.youtube.com/watch?v=8CmvdhbzhKI"
}

Retour :
{
    "tokens": [
        {
            "token": "Speedrun de Goat Simulator disponible ici : https://www.youtube-redirector.com/video=8CmvdhbzhKI",
            "start_offset": 0,
            "end_offset": 87,
            "type": "word",
            "position": 0
        }
    ]
}
```

# Tokenizer

Liste des **Tokenizers** disponibles dans ES :
- Word Oriented (texte vers mot individuel)
    - Standard, découpe par espace et supprime les symboles (sauf l'apostrophe)
    - Lowercase comme Letter mais ajoute le lowercase
    - UAX URL Email, comme le Standard mais les url et mails sont des jetons uniques.
- Partial Word (texte vers petit fragment de mot pour les correspondances partielles)
    - N-Gram ; Edge N-Gram
- Structured Text (utilisé pour les e-mails, codes postaux, ...)
    - Path, split la structure hiérarchique et émet un terme pour chaque.



## Standard

```
POST {{index}}/_analyze
{
	"tokenizer": "standard",
	"text": "Speedrun ; fléau ou pas ? Aujourd'hui, une réponse !"
}

Retour :
{
    "tokens": [
        {
            "token": "Speedrun",
            "start_offset": 0,
            "end_offset": 8,
            "type": "<ALPHANUM>",
            "position": 0
        },
        {
            "token": "fléau",
            "start_offset": 11,
            "end_offset": 16,
            "type": "<ALPHANUM>",
            "position": 1
        },
        {
            "token": "ou",
            "start_offset": 17,
            "end_offset": 19,
            "type": "<ALPHANUM>",
            "position": 2
        },
        {
            "token": "pas",
            "start_offset": 20,
            "end_offset": 23,
            "type": "<ALPHANUM>",
            "position": 3
        },
        {
            "token": "Aujourd'hui",
            "start_offset": 26,
            "end_offset": 37,
            "type": "<ALPHANUM>",
            "position": 4
        },
        {
            "token": "une",
            "start_offset": 39,
            "end_offset": 42,
            "type": "<ALPHANUM>",
            "position": 5
        },
        {
            "token": "réponse",
            "start_offset": 43,
            "end_offset": 50,
            "type": "<ALPHANUM>",
            "position": 6
        }
    ]
}
```

## Letter

```
POST {{index}}/_analyze
{
	"tokenizer": "letter",
	"text": "Speedrun ; fléau ou pas ? Aujourd'hui, une réponse !"
}

Retour :
{
    "tokens": [
        {
            "token": "Speedrun",
            "start_offset": 0,
            "end_offset": 8,
            "type": "word",
            "position": 0
        },
        {
            "token": "fléau",
            "start_offset": 11,
            "end_offset": 16,
            "type": "word",
            "position": 1
        },
        {
            "token": "ou",
            "start_offset": 17,
            "end_offset": 19,
            "type": "word",
            "position": 2
        },
        {
            "token": "pas",
            "start_offset": 20,
            "end_offset": 23,
            "type": "word",
            "position": 3
        },
        {
            "token": "Aujourd",
            "start_offset": 26,
            "end_offset": 33,
            "type": "word",
            "position": 4
        },
        {
            "token": "hui",
            "start_offset": 34,
            "end_offset": 37,
            "type": "word",
            "position": 5
        },
        {
            "token": "une",
            "start_offset": 39,
            "end_offset": 42,
            "type": "word",
            "position": 6
        },
        {
            "token": "réponse",
            "start_offset": 43,
            "end_offset": 50,
            "type": "word",
            "position": 7
        }
    ]
}
```

## Lowercase

```
POST {{index}}/_analyze
{
	"tokenizer": "lowercase",
	"text": "Speedrun ; fléau ou pas ? Aujourd'hui, une réponse !"
}

Retour :
{
    "tokens": [
        {
            "token": "speedrun",
            "start_offset": 0,
            "end_offset": 8,
            "type": "word",
            "position": 0
        },
        {
            "token": "fléau",
            "start_offset": 11,
            "end_offset": 16,
            "type": "word",
            "position": 1
        },
        {
            "token": "ou",
            "start_offset": 17,
            "end_offset": 19,
            "type": "word",
            "position": 2
        },
        {
            "token": "pas",
            "start_offset": 20,
            "end_offset": 23,
            "type": "word",
            "position": 3
        },
        {
            "token": "aujourd",
            "start_offset": 26,
            "end_offset": 33,
            "type": "word",
            "position": 4
        },
        {
            "token": "hui",
            "start_offset": 34,
            "end_offset": 37,
            "type": "word",
            "position": 5
        },
        {
            "token": "une",
            "start_offset": 39,
            "end_offset": 42,
            "type": "word",
            "position": 6
        },
        {
            "token": "réponse",
            "start_offset": 43,
            "end_offset": 50,
            "type": "word",
            "position": 7
        }
    ]
}
```

## Whitespace

```
POST {{index}}/_analyze
{
	"tokenizer": "whitespace",
	"text": "Speedrun ; fléau ou pas ? Aujourd'hui, une réponse !"
}

Retour :
{
    "tokens": [
        {
            "token": "Speedrun",
            "start_offset": 0,
            "end_offset": 8,
            "type": "word",
            "position": 0
        },
        {
            "token": ";",
            "start_offset": 9,
            "end_offset": 10,
            "type": "word",
            "position": 1
        },
        {
            "token": "fléau",
            "start_offset": 11,
            "end_offset": 16,
            "type": "word",
            "position": 2
        },
        {
            "token": "ou",
            "start_offset": 17,
            "end_offset": 19,
            "type": "word",
            "position": 3
        },
        {
            "token": "pas",
            "start_offset": 20,
            "end_offset": 23,
            "type": "word",
            "position": 4
        },
        {
            "token": "?",
            "start_offset": 24,
            "end_offset": 25,
            "type": "word",
            "position": 5
        },
        {
            "token": "Aujourd'hui,",
            "start_offset": 26,
            "end_offset": 38,
            "type": "word",
            "position": 6
        },
        {
            "token": "une",
            "start_offset": 39,
            "end_offset": 42,
            "type": "word",
            "position": 7
        },
        {
            "token": "réponse",
            "start_offset": 43,
            "end_offset": 50,
            "type": "word",
            "position": 8
        },
        {
            "token": "!",
            "start_offset": 51,
            "end_offset": 52,
            "type": "word",
            "position": 9
        }
    ]
}
```

## UAX URL Email

```
POST {{index}}/_analyze
{
	"tokenizer": "uax_url_email",
	"text": "Speedrun : https://www.speedrun.com/Celeste ; fléau ou pas ? Aujourd'hui, john@doe.com nous apporte une réponse !"
}

Retour :
{
    "tokens": [
        {
            "token": "Speedrun",
            "start_offset": 0,
            "end_offset": 8,
            "type": "<ALPHANUM>",
            "position": 0
        },
        {
            "token": "https://www.speedrun.com/Celeste",
            "start_offset": 11,
            "end_offset": 43,
            "type": "<URL>",
            "position": 1
        },
        {
            "token": "fléau",
            "start_offset": 46,
            "end_offset": 51,
            "type": "<ALPHANUM>",
            "position": 2
        },
        {
            "token": "ou",
            "start_offset": 52,
            "end_offset": 54,
            "type": "<ALPHANUM>",
            "position": 3
        },
        {
            "token": "pas",
            "start_offset": 55,
            "end_offset": 58,
            "type": "<ALPHANUM>",
            "position": 4
        },
        {
            "token": "Aujourd'hui",
            "start_offset": 61,
            "end_offset": 72,
            "type": "<ALPHANUM>",
            "position": 5
        },
        {
            "token": "john@doe.com",
            "start_offset": 74,
            "end_offset": 86,
            "type": "<EMAIL>",
            "position": 6
        },
        {
            "token": "nous",
            "start_offset": 87,
            "end_offset": 91,
            "type": "<ALPHANUM>",
            "position": 7
        },
        {
            "token": "apporte",
            "start_offset": 92,
            "end_offset": 99,
            "type": "<ALPHANUM>",
            "position": 8
        },
        {
            "token": "une",
            "start_offset": 100,
            "end_offset": 103,
            "type": "<ALPHANUM>",
            "position": 9
        },
        {
            "token": "réponse",
            "start_offset": 104,
            "end_offset": 111,
            "type": "<ALPHANUM>",
            "position": 10
        }
    ]
}
```

# Token Filter

Liste des **Token Filter** disponibles dans ES :
- Lowercase ; Uppercase
- NGram ; Edge NGram
- Stop, enlève les stop word
- Word delimiter, split les mots en sous-mot (ex: Wi-Fi en Wi et Fi, PowerShell en Power et Shell, CE1000 en CE et 1000)
- Stemmer, ne conserve que les radicaux des mots
- Keyword Marker, protéger certains mots d'être modifés par les Stemmers
- Snowball, ne conserve que les radicaux des mots suivant l'algorithme [Snowball](http://snowball.tartarus.org/texts/introduction.html)
- Synonym, remplace les tokens suivant un listing de synonymes
- Trim
- Length
- Truncate

## Length Token Filter

```
PUT {{index}}
{
	"settings": {
		"analysis": {
			"filter" : {
                "length_filter" : {
                    "type": "length",
                    "max": 5
                }
            }
		}
	}
}

POST {{index}}/_analyze
{
	"tokenizer": "standard",
	"filter": [
		"length_filter"
	],
	"text": "Speedrun ; fléau ou pas ? Aujourd'hui, une réponse !"
}

Retour :
{
    "tokens": [
        {
            "token": "fléau",
            "start_offset": 11,
            "end_offset": 16,
            "type": "<ALPHANUM>",
            "position": 1
        },
        {
            "token": "ou",
            "start_offset": 17,
            "end_offset": 19,
            "type": "<ALPHANUM>",
            "position": 2
        },
        {
            "token": "pas",
            "start_offset": 20,
            "end_offset": 23,
            "type": "<ALPHANUM>",
            "position": 3
        },
        {
            "token": "une",
            "start_offset": 39,
            "end_offset": 42,
            "type": "<ALPHANUM>",
            "position": 5
        }
    ]
}
```

---
name: analyzer-inverted-index

# Analyzer

.left-column[
#### .gray[Schéma]
#### Inverted Index
]

.right-column[
L'**Inverted Index** va être l'équivalent à l'index des mots à la fin d'un livre (tel mot est visible à telles pages).

|Term         |Document #1|Document #2|
|-------------|:---------:|:---------:|
|techshare    |     X     |           |
|elasticsearch|     X     |           |
|sandbox      |           |     X     |
]

---
name: mapping-dynamic

## Mapping

.left-column[
#### Mapping dynamique
]

.right-column[
Valeurs possibles :

- true (défaut) :
    - Active l'ajout de champs non déclarés dans un mapping
    - Les champs du document sont stockés et indexés
- false :
    - Ignore les nouveaux champs
    - Les champs du document sont stockés mais non indexés
- strict :
    - Lance une erreur (et le document n'est pas inséré)

Cette option peut être déclarée à n'importe quel niveau de profondeur dans la déclaration des champs.

💡 Placer le mapping dynamique à "false" est un bon compromis.
]

???

## Mapping dynamique mis à "true"

1] Création de l'index

```
PUT {{index}}
{
	"mappings": {
        "default": {
            "dynamic": "true"
        }
    }
}
```

2] Ajout d'un document

```
POST {{index}}/{{type}}/1
{
	"name": "Elasticsearch"
}
```

3] Le document peut être recherché

```
GET {{index}}/{{type}}/_search
{
    "query": {
        "match": {
            "name": "Elasticsearch"
        }
    }
}

Retour :
{
    "took": 1,
    "timed_out": false,
    "_shards": {
        "total": 5,
        "successful": 5,
        "failed": 0
    },
    "hits": {
        "total": 1,
        "max_score": 0.2876821,
        "hits": [
            {
                "_index": "speedrun",
                "_type": "default",
                "_id": "1",
                "_score": 0.2876821,
                "_source": {
                    "name": "Elasticsearch"
                }
            }
        ]
    }
}
```

## Mapping dynamique mis à "false"

1] Création de l'index

```
PUT {{index}}
{
	"mappings": {
        "default": {
            "dynamic": "false"
        }
    }
}
```

2] Ajout d'un document

```
POST {{index}}/{{type}}/1
{
	"name": "Elasticsearch"
}
```

3] Le document ne peut pas être recherché

```
GET {{index}}/{{type}}/_search
{
    "query": {
        "match": {
            "name": "Elasticsearch"
        }
    }
}

Retour :
{
    "took": 1,
    "timed_out": false,
    "_shards": {
        "total": 5,
        "successful": 5,
        "failed": 0
    },
    "hits": {
        "total": 0,
        "max_score": null,
        "hits": []
    }
}
```

4] Par contre, le document apparaîtra dans une requête "match_all"

```
GET {{index}}/{{type}}/_search
{
    "query": {
        "match_all": {}
    }
}

Retour :
{
    "took": 0,
    "timed_out": false,
    "_shards": {
        "total": 5,
        "successful": 5,
        "failed": 0
    },
    "hits": {
        "total": 1,
        "max_score": 1,
        "hits": [
            {
                "_index": "speedrun",
                "_type": "default",
                "_id": "1",
                "_score": 1,
                "_source": {
                    "name": "Elasticsearch"
                }
            }
        ]
    }
}
```

## Mapping dynamique mis à "strict"

1] Création de l'index

```
PUT {{index}}
{
	"mappings": {
        "default": {
            "dynamic": "strict"
        }
    }
}
```

2] Ajout d'un document ...

```
POST {{index}}/{{type}}/1
{
	"name": "Elasticsearch"
}
```

3] Le document ne peut pas être ajouté car le champs "name" n'est pas mappé

```
{
    "error": {
        "root_cause": [
            {
                "type": "strict_dynamic_mapping_exception",
                "reason": "mapping set to strict, dynamic introduction of [name] within [default] is not allowed"
            }
        ],
        "type": "strict_dynamic_mapping_exception",
        "reason": "mapping set to strict, dynamic introduction of [name] within [default] is not allowed"
    },
    "status": 400
}
```

---
name: mapping-types

## Mapping

.left-column[
#### .gray[Mapping dynamique]
#### Types
]
.right-column[
#### Quelques types disponibles

- Core (Text, Keyword, Integer, Date, Boolean, ...)
- Geo (Geo-point, ...)
- Specialized (IP, Attachment, ...)
- Complex (Object [1], Array [2])

#### Quelques options disponibles

- **coerce** : cast automatique
- **format** : format pour les dates (`yyyy-MM-dd`, `epoch_millis`, ...)
- **null_value** : valeur à placer si null est rencontré. N'affecte pas _source mais uniquement la valeur indexée.
]

???

[1] Lors de l'indexation :

```
{
    "game": {
        "name": "Castlevania - Aria of Sorrow",
        "platform": "GBA"
    }
}
```

Stockage :

```
{
    "game.name": "Castlevania - Aria of Sorrow",
    "game.platform": "GBA"
}
```

[2] Les associations des objets sont perdues dans les tableaux (à cause de Lucene qui ne connait pas la notion d'inner objects)

Indexation :

```
{
    "players": [
        { "country": "JP", "name": "nero_x2" },
        { "country": "DE", "name": "OddBod" }
    ]
}
```

Stockage :

```
{
    "players.country": [ "DE", "JP" ],
    "players.name": ["nero_x2", "OddBod"]
}
```

- Tout champs peut être un tableau (de même type) par défaut
- Le type **Nested** est une solution pour la perte des relations des objets dans un tableau

---
name: mapping-tips

## Mapping

.left-column[
#### .gray[Mapping dynamique]
#### .gray[Types]
#### Tips
]

.right-column[
💡 Désactiver, si possible, le champs qui permet de rechercher à travers tous les champs

```
PUT {{index}}
{
	"_all": {
		"enabled": false
	}
}
```

💡 Eviter d'avoir un underscore comme premier caractère, vous pourriez écrire le nom d'un [meta-field](https://www.elastic.co/guide/en/elasticsearch/reference/5.4/mapping-fields.html) et provoquer une erreur
]

???

Erreur lancée si un des meta-field est inséré dans un document (sauf `_meta`) :

```
{
    "error": {
        "root_cause": [
            {
                "type": "mapper_parsing_exception",
                "reason": "Cannot generate dynamic mappings of type [_field_names] for [_field_names]"
            }
        ],
        "type": "mapper_parsing_exception",
        "reason": "Cannot generate dynamic mappings of type [_field_names] for [_field_names]"
    },
    "status": 400
}
```

Exemple d'utilisation du _all :

```
{
	"query": {
		"match": {
			"_all": "Any%"
		}
	}
}
```

---
name: recapitulatif-1

## Récapitulatif

.left-column[
### Partie I
]

.red[.right-column[
- Analyzer
    - Schéma
    - Inverted Index

- Mapping
    - Mapping dynamique
    - Types
    - Tips
]]

---
name: recherches-query-vs-filter

## Recherches

.left-column[
#### Query vs Filter
]

.right-column[
Une **Query** répond à la question `"Comment précisément les documents répondent-ils à ma requête ?"`

- La pertinence (score) est prise en compte

Un **Filter** répond à la question `"Ais-je des document qui répondent à ma requête ?"`

- Aucune pertinence n'est calculée, il répond juste oui ou non

- Les résultats des `Filters` sont dans certains cas mis en cache par ES ([source](https://www.elastic.co/guide/en/elasticsearch/reference/5.4/query-cache.html)).

💡 Préférez utiliser les filters que les query afin de ne pas calculer de score
]

---
name: recherches-scoring

## Recherches

.left-column[
#### .gray[Query vs Filter]
#### Scoring
]

.right-column[
#### Algorithme de scoring

Avant 5.0 : TF/IDF ([source](https://fr.wikipedia.org/wiki/TF-IDF))

Depuis 5.0 : Okapi BM25 ([source](https://fr.wikipedia.org/wiki/Okapi_BM25))

#### Afficher les informations de scoring (debug)

`GET {{index}}/{{type}}/_search?explain`

#### Booster le scoring pour certains champs

- Pour apporter un score plus pertinent pour certains champs, la clé `boost` peut être appliqué sur celui-ci lors d'une requête.
- Il est possible d'ajouter un boost lors du mapping d'un champs mais ce n'est pas considéré comme une bonne pratique. ([source](https://www.elastic.co/guide/en/elasticsearch/reference/5.4/mapping-boost.html))
]

???

## TF/IDF

- **TF** : combien de fois un terme apparaît dans le document (plus il apparaît plus le score est élevé).
- **IDF** : à quelle fréquence apparaît le terme dans l'index (plus il apparaît moins le score est élevé).
- **Field-length norm** : plus le champs est long moins il est pertinent (ex: plus un titre est long moins un terme n'a d'importance)

Les trois sont calculés et stockés à l'indexation et permettent de calculer le poids d'un terme dans une requête.

## Okapi BM25

Il utilise la même technique mais a une meilleure gestion des stop words.

Il améliore le facteur **Field-length norm** en permettant de le configurer.

## Boost

```
POST _search
{
    "query": {
        "match" : {
            "game.name": {
                "query": "Celeste",
                "boost": 5
            }
        }
    }
}
```

---
name: recherches-requete-booleenne

## Recherches

.left-column[
#### .gray[Query vs Filter]
#### .gray[Scoring]
#### Requête booléenne
]

.right-column[
Si un must et un should sont présents, la condition du should :
- ne doit pas obligatoirement être respectée (sauf avec l'option **minimum_should_match** > 0)
- si elle est respectée, le document aura un meilleur score

Si aucun must n'est présent, la condition du should devient obligatoire.
]

???

## Exemple de requête booléenne

```
{
	"query": {
		"bool": {
			"must": [
				{
					"match": {
						"category": "All Red Berries"
					}
				}
			],
			"should": [
				{
					"term": {
						"game.name.raw": "Celeste"
					}
				}
			],
            "filter": [
                {
                    "range": {
                        "time": {
                            "gte": 3000
                        }
                    }
                }
            ]
		}
	}
}
```

---
name: recherches-slop

## Recherches

.left-column[
#### .gray[Query vs Filter]
#### .gray[Scoring]
#### .gray[Requête booléenne]
#### Slop
]

.right-column[
Si l'on accepte qu'un certain nombre de termes soient présent entre ceux de la recherche alors l'option **slop** sera utilisé.

Plus la proximité des mots de la recherche est proche, plus le scoring sera élevé.

##### Exemple, "Super Mario Odyssey" vient d'être indexé, cette requête le trouvera
```
GET {{index}}/{{type}}/_search
{
    "query": {
        "match_phrase": {
        	"game.name": {
        		"query": "Super Odyssey",
        		"slop": 1	
        	}
        }
    }
}
```
]

???

## Exemple

1] Création de l'index

```
PUT {{index}}
{
	"mappings": {
        "default": {
            "properties": {
                "game": {
                    "properties": {
                        "name": {
                            "type": "text",
                            "analyzer": "standard"
                        }
                    }
                }
            }
        }
    }
}
```

2] Ajout d'un document

```
POST {{index}}/{{type}}/1
{
    "game": {
    	"name": "Super Mario Odyssey"
    }
}
```

3] Recherche avec `match_phrase` qui fonctionne

```
GET {{index}}/{{type}}/_search
{
    "query": {
        "match_phrase": {
        	"game.name": "Super Mario"
        }
    }
}
```

4] Recherche avec `match_phrase` qui fonctionne qu'avec `slop`

```
GET {{index}}/{{type}}/_search
{
    "query": {
        "match_phrase": {
        	"game.name": {
        		"query": "Super Odyssey",
        		"slop": 1	
        	}
        }
    }
}
```

5] Quand le `slop` atteint la fin de la valeur, il revient au début de celle-ci et continue, donnant lieu à ce type de résutat

```
GET {{index}}/{{type}}/_search
{
    "query": {
        "match_phrase": {
        	"game.name": {
        		"query": "Mario Super",
        		"slop": 2
        	}
        }
    }
}
```

---
name: recherches-fuzzyness

## Recherches

.left-column[
#### .gray[Query vs Filter]
#### .gray[Scoring]
#### .gray[Requête booléenne]
#### .gray[Slop]
#### Fuzziness
]

.right-column[
Si l'on désire retourner des résultats malgré une faute de frappe alors l'option **fuzziness** sera utilisée.

##### Exemple, "Celeste" vient d'être indexé, cette requête le trouvera

```
GET {{index}}/{{type}}/_search
{
    "query": {
        "fuzzy": {
            "game.name": {
                "value": "Celaste",
                "fuzziness": 1
            }
        }
    }
}
```
]

???

La [distance de Leveinstein](https://fr.wikipedia.org/wiki/Distance_de_Levenshtein) et les
[transpositions](https://fr.wikipedia.org/wiki/Distance_de_Damerau-Levenshtein) sont utilisées pour le calcul du fuzzyness (erreurs de frappes).

80% des recherches utilisateurs peuvent être corrigées avec seulement une ou deux opérations.
 
Plus d'opérations réduirait les performances et peut donner lieu à des résultats imprévus.

Sommaire (pour la valeur "auto" de "fuzziness") :

|Longueur du terme|Distance maximale|
|-----------------|-----------------|
|1-2              |0                |
|3-5              |1                |
|>5               |2                |

---
name: recherches-query-string

## Recherches

.left-column[
#### .gray[Query vs Filter]
#### .gray[Scoring]
#### .gray[Requête booléenne]
#### .gray[Slop]
#### .gray[Fuzziness]
#### Query String
]

.right-column[
Il est possible d'écrire des requêtes directement dans la [Query String](https://www.elastic.co/guide/en/elasticsearch/reference/5.4/query-dsl-query-string-query.html).

- GET _search?q=*
- GET _search?q=time:3097
- GET _search?date=2019-01-01<u>%20</u>AND<u>%20</u>time:3097

💡 Pour avoir une belle sortie json, vous pouvez :
- Faire appel à json_pp (si installé) : `curl ... | json_pp`
- Ajouter pretty dans la requête : `?q=*&pretty`

Equivalence de la query string sous forme json :

```
{
	"query": {
		"query_string": {
			"query": "time:3097"
		}
	}
}
```
]

---
name: recherches-informations-diverses

## Recherches

.left-column[
#### .gray[Query vs Filter]
#### .gray[Scoring]
#### .gray[Requête booléenne]
#### .gray[Slop]
#### .gray[Fuzziness]
#### .gray[Query String]
#### Informations diverses
]

.right-column[
#### Match vs Term

- **Match** analyse la query de recherche
- **Term** n'analyse pas la query de recherche

Exemple, si le mot <u>speedrun</u> est un **term** de l'**inverted index**, alors la recherche du mot "Speedrun" :
- donnera un résultat avec un **match** (Speedrun donne le **term** speedrun après passage de l'analyzer)
- ne donnera pas de résultat avec un **term** (Speedrun donne le **term** Speedrun <- texte non analysé)
]

???

## Match

La requête `match` :
- Est un raccourci pour effectuer une requête booleéenne
- Analyse la recherche et combine tous les termes dans un must ou un should (suivant l'opérateur)

Exemple avec `or` (opérateur par défaut, ici "operator" pourrait être enlevé) :

```
{
	"query": {
		"match": {
			"game.name": {
				"query": "Celeste Classic",
            	"operator": "or"
			}
		}
	}
}
```

équivaut à

```
{
	"query": {
		"bool": {
			"should": [
				{
					"term": {
						"game.name": "celeste"
					}
				},
				{
					"term": {
						"game.name": "classic"
					}
				}
			]
		}
	}
}
```

=> analyse la recherche et place tous les termes dans un `should`

Exemple avec `and`

```
{
	"query": {
		"match": {
			"game.name": {
				"query": "Celeste Classic",
            	"operator": "and"
			}
		}
	}
}
```

équivaut à

```
{
	"query": {
		"bool": {
			"must": [
				{
					"term": {
						"game.name": "celeste"
					}
				},
				{
					"term": {
						"game.name": "classic"
					}
				}
			]
		}
	}
}
```

=> analyse la recherche et place tous les termes dans un `must`

#### Match vs Term

Les résultats d'une recherche simple seront différents suivant un match et un term car match analyse la recherche contrairement à term.

1] Création de l'index

```
PUT {{index}}
{
	"mappings": {
        "default": {
            "properties": {
                "category": {
                    "type": "text",
                    "analyzer": "standard"
                }
            }
        }
    }
}
```

2] Ajout d'un document

```
POST {{index}}/{{type}}/1
{
    "category": "Any%"
}
```

3] Recherche avec `match` : 1 résultat

```
GET {{index}}/{{type}}/_search
{
	"query": {
		"match": {
			"category": "Any%"
		}
	}
}
```

4] Recherche avec `term` : aucun résultat

```
GET {{index}}/{{type}}/_search
{
	"query": {
		"term": {
			"category": "Any%"
		}
	}
}
```

#### Pagination

#### Size

Nombre de documents retournés par défaut : 10

Pour modifier ce nombre, placer { "size": ... } dans le corps de la requête

#### Offset

Offset par défaut : 0

Pour modifier ce nombre, placer { "from": ... } dans le corps de la requête

[Attention aux recherches profondes de documents](https://www.elastic.co/guide/en/elasticsearch/reference/5.4/search-request-search-after.html)

#### Tri

Tri par défaut : { "_score": "desc" }

Multiple tris : { "sort": [{"game.name": "asc"}, {"date": "desc"}] }

Tri, précédé d'une transformation (avg, sum, min, max, ...) : { "sort": [ { "time": { "order": "desc", "mode": "avg" } } ] } 

#### Requêtes nommées

Afin de savoir à quelle condition un document a répondu, placer un _name avec un nom pour ensuite le retrouver dans le résultat dans la clé `matched_queries`.

```
# Requête
GET {{index}}/{{type}}/_search
{
	"query": {
		"match": {
			"category": {
				"query": "Any%",
				"_name": "category-any"
			}
		}
	}
}

# Réponse
{
    "took": 3,
    "timed_out": false,
    "_shards": {
        "total": 5,
        "successful": 5,
        "failed": 0
    },
    "hits": {
        "total": 1,
        "max_score": 0.2876821,
        "hits": [
            {
                "_index": "speedrun",
                "_type": "default",
                "_id": "1",
                "_score": 0.2876821,
                "_source": {
                    "category": "Any%"
                },
                "matched_queries": [
                    "category-any"
                ]
            }
        ]
    }
}
```

#### Range

1] Création de l'index

```
PUT {{index}}
{
	"mappings": {
        "default": {
            "properties": {
                "date": {
                    "type": "date",
                    "format": "yyyy-MM-dd"
                }
            }
        }
    }
}
```

2] Ajout d'un document

```
POST {{index}}/{{type}}/1
{
    "date": "2019-02-10"
}
```

3] Recherche avec `range` (format convenu)

```
GET {{index}}/{{type}}/_search
{
	"query": {
		"range": {
			"date": {
				"gte": "2018-01-01"
			}
		}
	}
}
```

4] Recherche avec `range` (format désigné à la recherche)

```
GET {{index}}/{{type}}/_search
{
	"query": {
		"range": {
			"date": {
				"gte": "01-01-2018",
                "format": "dd-MM-yyyy"
			}
		}
	}
}
```

5] Recherche avec `range` (recherche relative à une date)

```
GET {{index}}/{{type}}/_search
{
	"query": {
		"range": {
			"date": {
				"gte": "2020-01-01||-1y"
			}
		}
	}
}
```

#### Prefix ; Wilcard ; RegExp

1] Création de l'index

```
PUT {{index}}
{
	"mappings": {
        "default": {
            "properties": {
                "game": {
                    "properties": {
                        "name": {
                            "type": "text",
                            "fields": {
                                "raw": {
                                    "type": "keyword"
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
```

2] Ajout d'un document

```
POST {{index}}/{{type}}/1
{
    "game": {
    	"name": "Zelda, breath of the wild"
    }
}
```

3] Recherche avec `prefix`

```
GET {{index}}/{{type}}/_search
{
	"query": {
		"prefix": {
			"game.name.raw": "Zelda"
		}
	}
}
```

4] Recherche avec `wilcard` (seuls les caractères * et ? sont autorisés, pas les expressions régulières)

```
GET {{index}}/{{type}}/_search
{
	"query": {
		"wildcard": {
			"game.name.raw": {
				"value": "Zelda? breath*"
			}
		}
	}
}
```

5] Recherche avec `regexp` (le moteur d'expression régulière est celui de Lucene)

```
GET {{index}}/{{type}}/_search
{
	"query": {
		"regexp": {
			"game.name.raw": {
				"value": "Zelda.( [a-z]+)+"
			}
		}
	}
}
```

---
name: recherches-query-string

## Recherches

.left-column[
#### .gray[Query vs Filter]
#### .gray[Scoring]
#### .gray[Requête booléenne]
#### .gray[Slop]
#### .gray[Fuzziness]
#### .gray[Query String]
#### .gray[Informations diverses]
#### Tips
]

.right-column[
💡 Lors de votre recherche, préciser autant que possible votre "_source" (pour limiter le poids de la sortie et savoir réellement quel champs vous avez besoin)

* Exemple d'inclusion / exclusion de champs :

```
{
	"_source": {
		"includes": [ "game.*", "link" ],
		"excludes": "game.platform"
	}
}
```
]

---
name: aggregations-single-value-numeric-metric-aggregations

## Aggrégations

.left-column[
#### Scalaires ; Buckets
]

.right-column[
Les aggrégations peuvent retourner :
- des valeurs scalaires (calcul d'une somme ou d'une moyenne, par exemple)
- des buckets ou ensemble de documents

Elles peuvent aussi créer des histogrammes.
]

???

## Exemple d'aggrégations retournant des scalaires

```
GET {{index}}/{{type}}/_search
{
	"size": 0,
	"aggs": {
		"total_time": {
			"sum": {
				"field": "time"
			}
		},
		"avg_time": {
			"avg": {
				"field": "time"
			}
		},
		"min_time": {
			"min": {
				"field": "time"
			}
		},
		"max_time": {
			"max": {
				"field": "time"
			}
		},
		"time_stats": {
			"stats": {
				"field": "time"
			}
		},
		"total_game_platforms": {
			"cardinality": {
				"field": "game.platform"
			}
		}
	}
}
```

???

## Mauvais comptage

Le comptage n'est pas précis à cause de la nature distribuée des index dans les shards.

En effet, si l'on demande la somme des trois meilleurs documents, celle-ci ne sera correcte.

Exemple :

| |Shard A|Shard B|Shard C|
|-|-------|-------|-------|
|1|A (50) |A (50) |A (50) |
|2|B (40) |B (40) |E (40) |
|3|C (30) |F (30) |F (30) |
|4|F (20) |C (20) |B (20) |
|5|D (10) |E (10) |C (10) |

Donne

| |Actuel |Correct |
|-|-------|--------|
|1|A (150)|A (150) |
|2|B (80) |B (100) |
|3|F (60) |F (80)  |

Du coup, pour avoir la somme, il faut aggréger sur plus de résultats mais ça devient plus long.
Un équilibre doit être trouvé.

Le souci ne se pose pas si un unique shard est alloué pour un index.

---
name: aggregations-multi-value-numeric-metric-aggregations

## Aggrégations

.left-column[
#### .gray[Scalaires ; Buckets]
#### Contexte
]

.right-column[
### Contexte hérité

Si une query (ou un filter) est présent au niveau d'une aggrégation alors cette dernière aura comme contexte les résultats de la première.

De même, si une aggrégation est à l'intérieur d'une aggrégation, cette dernière aura pour contexte les résultats de la première.

### Contexte global

Si la clé **global** est présente, alors une aggrégation n'aura pas de contexte hérité.
]

---
name: recapitulatif-2

## Récapitulatif

.left-column[
### Partie I
### Partie II
]

.red[.right-column[
- Recherches
    - Query vs Filter
    - Scoring
    - Requête booléenne
    - Slop
    - Fuzziness
    - Query String
    - Informations diverses
    - Tips

- Aggrégations
    - Scalaires ; Buckets
    - Contexte
]]

---
name: infrastructure-cluster-node-shard-replica
background-image: url(images/cluster-node-shard-replica.png)
class: center
---
name: infrastructure-cluster-node

## Infrastructure

.left-column[
#### .gray[Vue d'ensemble]
#### Cluster ; Node
]

.right-column[
Cluster :
- Ensemble de nodes
- Identifié par un nom unique (elasticsearch par défaut)

Node :
- Serveur / Instance contenant un ensemble de shards et de réplicas (💡 20 Go max pour un node)
- Somme des nodes == totalité de la donnée
- Chaque node participe à l’indexation et aux capacités de recherche du cluster
- Tous les noeuds savent gérer les requêtes HTTP (grâce à l’api REST que le cluster expose par défaut)
- Tous les nodes connaissent tous les nodes présents dans le cluster
- Un des node sera choisit comme le master node (celui qui coordonnera les modifications sur le cluster)
- Identifié par un nom unique (un uuid par défaut).
]

---
name: infrastructure-shard-replica

## Infrastructure

.left-column[
#### .gray[Vue d'ensemble]
#### .gray[Cluster ; Node]
#### Shard ; Replica
]

.right-column[
Shard :
- Contient une partie des documents indexés
- Par défaut, 5 shards seront assignés pour un index
- Un document se fait assigner pour un shard suite à un algorithme de routing

Replica :
- Permet la haute disponibilité (sert de backup lors des crashs hardware)
- N'est jamais placé dans le même node que son shard primaire
- Par défaut, un shard a un replica
]

???

## Nombre maximum de documents dans un shard

La limite est de 2.147.483.519 documents pour un shard. ([source](https://www.elastic.co/guide/en/elasticsearch/reference/5.5/_basic_concepts.html#getting-started-shards-and-replicas))

## Algorithme de routing
- Formule : shard = hash(routing) % total_primary_shards
- => routing = identifiant du document (customisable)
---
name: infrastructure-schema

## Infrastructure

.left-column[
#### .gray[Vue d'ensemble]
#### .gray[Cluster ; Node]
#### .gray[Shard ; Replica]
#### Schéma
]

.right-column[
<div class="mermaid">
graph TB
    subgraph Infrastructure
    C(Cluster) --> |contient| N(Node)
    N --> |contient| S(Shard)
    I(Index) --> |est divisé en| S
    D(Document) --> |est indexé dans| I
    N --> |contient| R(Replica)
    S -.-> R
    end
</div>
]

---
name: infrastructure-x-pack

## Elastic Stack

.left-column[
#### X-Pack
]

.right-column[
Pack de fonctionnalités ajoutées à ES et Kibana
- Sécurité (authentification et autorisation)
- Monitoring (pour ES, Logstash & Kibana : utilisation du CPU de la mémoire et de l'espace disque, entre autre)
- Alerting (pour ce que l’on veut, pas forcément quelque chose de l'Elastic Stack)
- Reporting (exporter les visuels et les dashboards Kibana en pdf et csv)
- ML (Machine Learning)
- Graph (effectuer des relations (ex: suggérer des articles suite à l'achat d'autres articles, suggestion de musique, ...))
- Elasticsearch SQL (écrire les requêtes ES en SQL)
]

---
name: infrastructure-kibana

## Elastic Stack

.left-column[
#### .gray[X-Pack]
#### Kibana
]

.right-column[
Plateforme de visualisation de données provenant d'ES (avec des Pie Charts, Line Charts et bien d'autres).

Ajoute une interface pour administrer certaines parties d’ES et Logstash comme l’authentification et autorisation.

<img src="https://www.elastic.co/fr/assets/bltc02a4b0eadbc0ed1/kibana-timeseries.jpg" width="500" height="300" />
]

---
name: infrastructure-beats

## Elastic Stack

.left-column[
#### .gray[X-Pack]
#### .gray[Kibana]
#### Beats
]

.right-column[
Collection de Data Shippers (petits agents, à but unique, posés sur le serveur qui envoient des infos à ES ou Logstash).

Exemple :
- Filebeat, pour collecter les fichiers de log et les envoyer à ES ou Logstash (comme ceux de nginx, apache ou mysql)
- Heartbeat, pour surveiller la disponibilité de ses services
]

---
name: infrastructure-logstash

## Elastic Stack

.left-column[
#### .gray[X-Pack]
#### .gray[Kibana]
#### .gray[Beats]
#### Logstash
]

.right-column[
Avant, il fut utilisé pour travailler sur des logs applicatif et les envoyer à ES, dorénavant c’est un data processing pipeline.

Les données reçues seront traitées comme des évènements, qui peuvent être de la forme de son choix
(fichiers de log, messages de chat, ...), et il peut les envoyer à plusieurs destinations
(ES, queue kafka, un mail ou un endpoint http).

La pipeline est constituée de 3 parties : inputs ; filters ; outputs, chacune de ses parties peuvent utiliser des plugins.
]

---
name: recapitulatif-3

## Récapitulatif

.left-column[
### Partie I
### Partie II
### Partie III
]

.red[.right-column[
- Infrastructure
    - Vue d'ensemble
    - Cluster ; Node
    - Shard ; Replica
    - Schéma

- Elastic Stack
    - X-Pack
    - Kibana
    - Beats
    - Logstash
]]

---
name: end

### Url du Techshare

* https://gitlab.com/s.robine/techshare-elasticsearch

<img src="http://www.fullmoonissue.net/fullmoonissue.png" width="350" height="420" />

???

## Quelques liens disparates

- [[/](https://github.com/dzharii/awesome-elasticsearch)] L'**awesome** dédié sur github
- [[/](http://elasticsearch-cheatsheet.jolicode.com/)] Cheatsheet de Jolicode
- [[/](https://lzone.de/cheat-sheet/ElasticSearch)] Cheatsheet de Lars Windolf
- [[/](https://github.com/fdv/running-elasticsearch-fun-profit)] Beaucoup d'informations autour du run d'ES
- [[/](https://www.elastic.co/guide/en/elasticsearch/reference/5.4/glossary.html)] Glossaire