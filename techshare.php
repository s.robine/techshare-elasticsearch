<?php

$html = <<<HTML
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Techshare ES 5.4</title>
        <style>
        .left-column {
            width: 20%;
            height: 92%;
            float: left;
        }
        .right-column {
            width: 75%;
            float: right;
            padding-top: 1em;
        }
        .split-column {
            width: 50%;
            float: left;
            padding-top: 1em;
        }
        .third-column {
            width: 33%;
            height: 82%;
            float: left;
            border-right: 1px solid #aaa;
        }
        .red {
            color: red;
        }
        .gray {
            color: gray;
        }
        .need-ref {
            border-bottom: 1px solid #aaa;
        }
        </style>
    </head>
    <body>
        <textarea id="source">
            [MD]
        </textarea>
        <script src="./assets/remarks.min.js"></script>
        <script src="./assets/mermaid.min.js"></script>
        <link rel="stylesheet" href="./assets/mermaid.css">
        <script>
            var slideshow = remark.create();
            
            // don't let mermaid automatically load on start
            mermaid.initialize({
                startOnLoad: false,
                cloneCssStyles: false
            });
            
            function initMermaidInSlide(slide) {
                var slideIndex = slide.getSlideIndex();
                // caution: no API to get the DOM element of current slide in remark, this might break in the future
                var currentSlideElement = document.querySelectorAll(".remark-slides-area .remark-slide")[slideIndex];
                var currentSlideMermaids = currentSlideElement.querySelectorAll(".mermaid");
                if (currentSlideMermaids.length !== 0) {
                    mermaid.init(undefined, currentSlideMermaids);
                }
            }
            
            // first starting slide won't trigger the slide event, manually init mermaid
            initMermaidInSlide(slideshow.getSlides()[slideshow.getCurrentSlideIndex()])
            // on each slide event, trigger init mermaid
            slideshow.on('afterShowSlide', initMermaidInSlide);
            
            var tables = document.querySelectorAll('table');
            for(var i in tables) {
                tables[i].setAttribute('border', '1')
                tables[i].setAttribute('cellpadding', '15')
            }
        </script>
    </body>
</html>
HTML;

file_put_contents(__DIR__.'/techshare.html', preg_replace('{\[MD\]}', file_get_contents(__DIR__.'/TECHSHARE.md'), $html));